const path = require("path");
const nodeExternals = require("webpack-node-externals");
const entry = { server: "./src/server/index.ts" };
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = {
  mode: process.env.NODE_ENV ? process.env.NODE_ENV : "development",
  target: "node",
  devtool: "inline-source-map",
  entry: entry,
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "server.js",
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"],
    plugins: [new TsconfigPathsPlugin({
      configFile: './tsconfig.server.json',
    })]
  },
  // don't compile node_modules
  externals: [nodeExternals()],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [
          {
            loader: "awesome-typescript-loader",
            options: {
              // use the tsconfig in the server directory
              configFile: "./tsconfig.server.json",
              useCache: true,
              "useBabel": true,
              "babelOptions": {
                  "babelrc": false, /* Important line */
                  "presets": [
                      ["@babel/preset-env", { "targets": "last 2 versions, ie 11", "modules": false }]
                  ]
              },
              "babelCore": "@babel/core", // needed for Babel v7
            },
          },
        ],
      },
    ],
  },
};
